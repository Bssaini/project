/**
 * Created by USER on 10/1/2017.
 */
var express = require('express');
const search = require('../controller/search');
var router = express.Router();

/* GET users listing. */
router.get('/', search.index);
router.post('/getFilters', search.getFilters);
router.post('/getMoreData', search.getMoreData);
module.exports = router;
